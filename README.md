# mini project 2


## Overview

This project implements a serverless AWS Lambda function using Rust for processing a dataset of husband and wife ages. It demonstrates efficient data analysis using AWS Lambda and showcases the integration with AWS API Gateway.

## Purpose

To create a serverless function on AWS Lambda for processing age data, allowing filtering based on user criteria. The function is accessible via HTTP requests through AWS API Gateway.

## Features

- Serverless Data Processing on AWS Lambda
- Rust Programming for efficient data handling
- AWS API Gateway Integration
- Dynamic Data Filtering of husband and wife ages
- Easy Invocation through command line or API Gateway

## Preparation and Deployment Guide

### Environment Setup
1. **Install Rust and Cargo Lambda**: 
   - `brew install rust`
   - `brew tap cargo-lambda/cargo-lambda`
   - `brew install cargo-lambda`

2. **AWS Account Setup**: 
   - Create an AWS account (free tier recommended).
   - In AWS IAM, create a user with the following policies: 
     - `AmazonEC2FullAccess`
     - `AmazonAPIGatewayInvokeFullAccess`
     - `AmazonAPIGatewayAdministrator`
     - `lambdafullaccess`
     - `iamfullaccess`
   - Securely store the generated access keys (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION).

### Project Configuration
1. **Initialize Project**: 
   - `cargo lambda new new-lambda-project`
2. **Customize the Template**: 
   - Include logic for processing the age data of husbands and wives.
   - Local testing: `cargo lambda watch`
   - Lambda function test: `cargo lambda invoke --data-ascii "{ \"filter\": 50.0 }"`

### Environment Variables
1. **Configure AWS Credentials**: 
   - Store credentials in a `.env` file (ensure it's in `.gitignore`).
   - Export AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_DEFAULT_REGION.

### Building and Deployment
1. **Build for Release**: 
   - `cargo lambda build --release`
2. **Deploy to AWS Lambda**: 
   - `cargo lambda deploy`
3. **Verify Deployment**: 
   - Check in the AWS Lambda console.

### API Gateway Integration
1. **Setup API Gateway**: 
   - Create REST API and resource.
   - Link the Lambda function.
   - Deploy the API with a new stage.
2. **API Testing**: 
   - Use the generated API Gateway URL for testing.
   - Example: `curl -X POST https://[api-gateway-url]/[stage]/[resource] -H 'content-type: application/json' -d '{ "filter": 50.0 }'`

### Continuous Integration
1. **GitLab CI/CD**: 
   - Add AWS credentials to GitLab secrets.
   - Use `.gitlab-ci.yml` for automated deployment.

### Lambda Function Testing
- Remotely test with `cargo lambda invoke --remote <lambda-function-name>`

## Expected Output

The Lambda function will output a filtered dataset based on the specified age filter. For example, if the filter is set to 50, the function will return a list of husband and wife pairs where the husband's age is greater than 50.

## Limitations and Future Improvements

- Enhanced error handling.
- Support for more complex data operations.
- Dynamic data sourcing.

## Conclusion

This project uses Rust in a serverless environment for processing a specific dataset, combining Rust's performance with AWS Lambda's scalability and the functionality of AWS API Gateway.
